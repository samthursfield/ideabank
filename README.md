# Idea Bank

Hello, this is my tool "Idea Bank" for importing and browsing audio snippets from storage on various audio devices.

This tool is in quite a half baked and prototypical state.

## Quick start

1. Clone this repo.

2. Install ideabank.

    pip3 install --user .

3. Adapt `config.toml` to your use case.

4. Connect device and run `$HOME/ideabank-venv/bin/ideabank import`. Repeat as needed.

5. Run `$HOME/ideabank-venv/bin/ideabank mount /tmp/ideas` (create a mountpoint at `/tmp/ideas` first).

6. Look in the filesystem for your ideas!

## Manual

### The config file

The config defines how content is imported from different devices.

Each device should have a "slug" name, and is defined in a table named `device.SLUG`.

The `type` key must be set to one of these values:

  * "storage" for a USB mass storage device
  * "mtp" for an MTP device such as a phone.

To identify a USB storage device, the `vendor` and `model` key should be set. You can discover these fields for a device when it's connected to your computer by running `lsusb -v`.

To identify an MTP device, the `name` key should be set.

By default all files are copied to the ideabank. To include only a subset, limit paths using the `include` key.

## The database

The database location is configured in the config file `config.path` key.

All imported content is saved into this database. Internally, content is managed using Git and Git LFS. This means that if the same file is imported multiple times, it is only saved once, so you can import the same thing multiple times without wasting disk space.

## The virtual filesystem

You can browse and organise audio snippets using the virtual file system.

There are three toplevel folders:

  * `devices/`: contains the content of each device at the last time you ran `import` for that device.
  * `all/`: contains all the audio files from devices/ in a single folder, with auto-generated names.
  * `labels/`: this folder is initially empty. Drag a file here from `all/` and you will be able to give it a meaningful name. Create folders to collect ideas together. You can create many copies of the same file 

## Device specific notes

MTP protocol (used by phones and stuff) is not currently supported, you can manually mount such devices. See instructions at <https://wiki.archlinux.org/title/Media_Transfer_Protocol>.

Example using android-file-transfer:

    aft-mtp-mount /tmp/phone
    ideabank import --device phone --path /tmp/phone
    fusermount -u /tmp/phone
