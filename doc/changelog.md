Changelog
=========

0.3a0
-----

 * Add `list` command
 * Rework internal data structures to speed up `mount` command.
 * Optimization in Git LFS store to list directory contents much faster.

0.2
---

 * First version using `git-lfs` storage backend.
