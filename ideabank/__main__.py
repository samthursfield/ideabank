import sys

from .ideabank import cli

try:
    cli()
except RuntimeError as e:
    if isinstance(e, NotImplementedError):
        raise e
    sys.stderr.write("ERROR: {}\n".format(e))
    sys.exit(1)
