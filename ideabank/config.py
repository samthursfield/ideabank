from pathlib import Path
from tomli import TOMLDecodeError
from typing import *
import dataclasses
import tomli


@dataclasses.dataclass
class DeviceConfig:
    name: str
    include: [str]

    match_vendor: str
    match_model: str


@dataclasses.dataclass
class Device:
    config: DeviceConfig
    drive_id: str
    mountpoints: [Path]


class ConfigError(RuntimeError):
    pass


class Config:
    DEFAULT_INCLUDE = ["*"]

    def __init__(self, path=None, repo_path=None):
        path = path or "./config.toml"
        with open(path, "rb") as f:
            try:
                self._dict = tomli.load(f)
            except TOMLDecodeError as e:
                raise RuntimeError(f"Error parsing {path}: {e}")

            try:
                repo_path = repo_path or self._dict["config"]["path"]
                self._repo_path = Path(repo_path)
            except KeyError:
                raise ConfigError(
                    "Repo path must be specified (use `config.path` key or `--repo` option)."
                )

    def devices(self) -> Iterable[Dict[str, DeviceConfig]]:
        result = {}
        for name, config in self._dict["device"].items():
            include = config.get("include", self.DEFAULT_INCLUDE)
            result[name] = DeviceConfig(
                name=name,
                include=include,
                match_vendor=config.get("vendor"),
                match_model=config.get("model"),
            )
        return result

    def get_device_by_name(self, name) -> DeviceConfig:
        return self.devices()[name]

    def repo_path(self) -> Path:
        return self._repo_path
