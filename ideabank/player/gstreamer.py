"""GStreamer-based player class."""

import gi

gi.require_version("Gst", "1.0")
from gi.repository import GLib, Gst

from io import BufferedReader
import logging
import sys

from .base import Player

log = logging.getLogger(__name__)


def install_excepthook():
    """Make sure we exit when an unhandled exception occurs."""
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk

    old_hook = sys.excepthook

    def new_hook(etype, evalue, etb):
        old_hook(etype, evalue, etb)
        while Gtk.main_level():
            Gtk.main_quit()
        sys.exit()

    sys.excepthook = new_hook


class GStreamerPlayer(Player):
    def __init__(self):
        Gst.init(None)
        install_excepthook()

        self._buffer = None
        self._pipeline = None

    def play(self, buffer: BufferedReader):
        self._buffer = buffer
        self._pipeline = Gst.parse_launch(
            "appsrc name=audio_src ! decodebin ! audioconvert ! autoaudiosink"
        )

        appsrc = self._pipeline.get_by_name("audio_src")
        appsrc.set_property("format", Gst.Format.TIME)
        appsrc.connect("need-data", self._push_data)

        self._pipeline.set_state(Gst.State.PLAYING)

        bus = self._pipeline.get_bus()
        while True:
            message = bus.timed_pop_filtered(
                Gst.CLOCK_TIME_NONE, Gst.MessageType.EOS | Gst.MessageType.ERROR
            )
            log.info("Got Gst message: %s", message.type)
            if message.type == Gst.MessageType.ERROR:
                error = message.parse_error()
                raise RuntimeError(error)
            if message.type == Gst.MessageType.EOS:
                break

        self._pipeline.set_state(Gst.State.NULL)

    def _push_data(self, appsrc, unused_length):
        data = self._buffer.read(4096)

        if not data:
            log.debug("End of stream")
            appsrc.emit("end-of-stream")
            return

        log.debug("Push buffer of length %i", len(data))
        gst_buffer = Gst.Buffer.new_wrapped_bytes(GLib.Bytes.new(data))
        appsrc.emit("push-buffer", gst_buffer)
