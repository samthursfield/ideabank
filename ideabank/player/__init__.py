from .base import Player

# The GStreamerPlayer class isn't imported here so that we don't pull in
# gobject-introspection until it's needed.
