"""The tree structure of the ideabank.

The tree nodes are created on demand by calling
:meth:`IdeaBankNode.populate`. A few toplevel nodes are prepopulated on
construction.

"""

from dataclasses import dataclass, field
from pathlib import Path
from typing import *

import logging

from .store import FileType, StoreObject


log = logging.getLogger(__name__)


def _child_path(path: Path) -> Path:
    parts = path.parts
    if len(parts) < 2:
        raise FileNotFoundError(f"Path {path} doesn't have a child path")
    return Path("/".join(parts[1:]))


@dataclass
class IdeaBankNode:
    """The base node type."""

    name: str
    parent: Optional[object] = None
    children: Dict[str, object] = field(default_factory=dict)
    depth: int = 0

    _populated: bool = False

    def __post_init__(self):
        if self.parent:
            self.depth = self.parent.depth + 1

    def __str__(self) -> str:
        return f"{self.__class__.__name__}({self.name})"

    def ensure_populated(self):
        if not self._populated:
            log.debug(f"Populate: {self}")
            self.do_populate()
            self._populated = True

    def get_child(self, path: str) -> Optional[object]:
        log.debug(f"{self}.get_child({path})")
        self.ensure_populated()

        result = None

        path = Path(path)
        if path.is_absolute():
            rel_path = path.relative_to("/")
        else:
            rel_path = path

        if len(rel_path.parts) == 0:
            result = self
        else:
            name = rel_path.parts[0]
            child = self.children.get(name)
            if len(rel_path.parts) == 1:
                result = child
            if len(rel_path.parts) > 1 and child:
                subpath = _child_path(rel_path)
                result = child.get_child(subpath)

        log.debug(f"{self}.get_child({path}): rel_path {rel_path}, result: {result}")
        return result

    def add_child(self, name: str, node: object):
        self.children[str(name)] = node

    def get_children(self) -> List[object]:
        self.ensure_populated()
        return list(self.children.keys())

    def export(self, max_depth: int, depth: int = 0) -> List[object]:
        self.ensure_populated()

        if max_depth and depth >= max_depth:
            return list(sorted(self.children.keys()))

        result = {}
        for name, node in self.children.items():
            result[name] = node.export(max_depth, depth=depth + 1)
        return result

    def do_populate(self):
        raise NotImplementedError(f"Node {self} doesn't implement `do_populate()`")

    def get_store_object(self) -> StoreObject:
        return None

    def is_file(self) -> bool:
        self.ensure_populated()
        return len(self.children) == 0


class Toplevel(IdeaBankNode):
    """The toplevel (root) node."""

    def __init__(self, store, config):
        IdeaBankNode.__init__(self, parent=None, name="", _populated=True)

        devices_root = DevicesRoot(self, store, config.devices())
        union_store_object = store.get_root("union")
        ideas_root = StoreNode(self, name="ideas", store_object=union_store_object)
        labels_root = LabelsRoot(self)
        self.add_child("devices", devices_root)
        self.add_child("ideas", ideas_root)
        self.add_child("labels", labels_root)


class DevicesRoot(IdeaBankNode):
    """The toplevel `devices` node."""

    def __init__(self, parent, store, devices):
        IdeaBankNode.__init__(self, parent=parent, name="devices", _populated=True)

        for device_name in devices:
            ref = f"refs/heads/devices/{device_name}"
            if store.ref_exists(ref):
                store_object = store.get_root(ref)
                device_toplevel = StoreNode(
                    parent=self, name=device_name, store_object=store_object
                )
                self.add_child(device_name, device_toplevel)


class LabelsRoot(IdeaBankNode):
    """The toplevel `labels` node."""

    def do_populate(self):
        pass


class StoreNode(IdeaBankNode):
    """A node that is backed by an object in the Ideabank Store.."""

    def __init__(self, parent, name, store_object):
        IdeaBankNode.__init__(self, parent=parent, name=name)

        self._store_object = store_object

    def do_populate(self):
        for child in self._store_object.list_tree():
            name = child.get_path().name
            node = StoreNode(
                parent=self,
                name=child,
                store_object=child,
            )
            self.add_child(name, node)

    def is_file(self) -> bool:
        return self._store_object.is_file()

    def export(self, max_depth: int, depth: int = 0) -> List[object]:
        self.ensure_populated()

        if max_depth and depth >= max_depth:
            return list(sorted(self.children.keys()))

        dirs = {}
        files = []
        for name, node in self.children.items():
            if node.is_file():
                files.append(name)
            else:
                dirs[name] = node.export(max_depth, depth=depth + 1)
        dirs.update({"files": files})
        return dirs

    def get_store_object(self) -> StoreObject:
        return self._store_object
