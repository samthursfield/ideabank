# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

"""Abstract base classes for Ideabank Store implementations."""


class StoreError(RuntimeError):
    pass


class Store:
    """Generic base class for content store."""

    pass


class StoreObject:
    """Generic base class for a stored object."""

    pass
