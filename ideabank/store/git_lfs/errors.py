# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later


from ideabank.store import StoreError


class GitError(StoreError):
    pass
