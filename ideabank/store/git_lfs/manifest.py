# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

"""Manifests, used to optimize performance of the Git LFS store.

Each ref in the store has a `manifest.json` file committed at the root which
lists the contents of the stored tree. This means we can fetch the tree with a
single file read.

Without the manifest, we would need to read each individual object and parse
the stored Git LFS data in order to find the file size and content hash.

"""

from pathlib import Path
from typing import List
import dataclasses
import json

from .errors import GitError
from .git_lfs_file_info import GitLfsFileInfo
from ideabank.store import FileType


@dataclasses.dataclass
class Manifest:
    entries: List[GitLfsFileInfo] = None

    @staticmethod
    def load(text):
        """Load JSON manifest from `text`.

        No validation is done on the input.

        """
        manifest = Manifest()

        data = json.loads(text)
        if data["version"] != 1:
            raise GitError(f"Unsupported manifest version: {data['version']}")

        manifest.entries = []
        for entry_info in data["contents"]:
            entry = GitLfsFileInfo(
                path=Path(entry_info["path"]),
                file_type=FileType(entry_info["file_type"]),
                size=entry_info["size"],
                content_hash=entry_info["content_hash"],
            )

            manifest.entries.append(entry)
        return manifest

    def save(self, path):
        """Save JSON manifest to `path`."""

        data = {
            "version": 1,
            "contents": [entry.export() for entry in self.entries],
        }
        with open(path, "w") as f:
            json.dump(data, f)

    def get_root_entry(self) -> GitLfsFileInfo:
        # There is no root entry returned by `git ls-tree`, but we can
        # fabricate one.
        return GitLfsFileInfo(
            path=Path("./"),
            file_type=FileType.DIRECTORY,
            size=0,
            content_hash="fixme",
        )
