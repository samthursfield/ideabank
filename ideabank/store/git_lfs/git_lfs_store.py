# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from typing import Iterable
import fnmatch
import logging
import os

from ideabank.config import DeviceConfig
from ideabank.store import FileType, Store, StoreObject
from .helpers import GitLfsHelper
from .git_lfs_file import GitLfsFile
from .git_lfs_root import GitLfsRoot
from .manifest import Manifest

log = logging.getLogger(__name__)


def _should_import_file_for_device(device_config: DeviceConfig, path: Path) -> bool:
    for pattern in device_config.include:
        if fnmatch.fnmatch(path, pattern):
            log.debug(f"Include {path} due to pattern {pattern}")
            return True
    log.debug(f"Rejected {path} after checking {len(device_config.include)} patterns")
    return False


def device_ref(device_name) -> str:
    return f"refs/heads/devices/{device_name}"


def filename_for_union(device_name, file_path) -> str:
    parts = [device_name.lower()] + list(file_path.parts)
    return "-".join(p.lower() for p in parts)


class GitLfsStore(Store):
    """Idea bank backend using git-lfs."""

    def __init__(self, path):
        self.path = path

        self.git = GitLfsHelper(
            path, user_name="ideabank", user_email="ideabank@example.com"
        )

    def ensure_exists(self):
        """Create store if missing."""
        if self.path.joinpath("config").exists():
            log.debug(f"Store exists at {self.path}")
        else:
            self.git.create_repo()

            self.git.setup_repo(
                # FIXME: we should only commit files matching this pattern
                # FIXME this pattern should be defined in config, and we should
                # update the config branch whenever it changes...
                track_types=["*.aif", "*.flac", "*.m4a", "*.wav", "*.WAV"],
                # All branches should be created from this one.
                config_branch_name="config",
            )

    def list_tree(
        self, ref_or_commit: str, subdir="./", recursive=False
    ) -> Iterable[StoreObject]:
        """List all paths in the tree that `ref_or_commit` points to.

        Hidden files are not returned by this method.

        Raises FileNotFoundError if `ref_or_commit` is not found.

        """
        root = self.get_root(ref_or_commit)
        return root.list_tree(subdir, recursive)

    def get_root(self, ref_or_commit):
        manifest = self._get_manifest(ref_or_commit)

        return GitLfsRoot(
            git=self.git,
            info=manifest.get_root_entry(),
            ref=ref_or_commit,
            root=None,
            manifest=manifest,
        )

    def _get_manifest(self, ref_or_commit) -> Manifest:
        obj = f"{ref_or_commit}:manifest.json"
        text = self.git.run(["cat-file", "blob", obj])
        return Manifest.load(text)

    def ref_exists(self, ref) -> bool:
        return self.git.ref_exists(ref)

    def import_from_device(self, device_config: DeviceConfig, path: Path):
        log.info(f"Importing from {device_config.name} at {path}")

        files = []
        for dirpath, dirnames, filenames in os.walk(path):
            # Evaluate file against device config
            for filename in filenames:
                candidate = Path(dirpath).joinpath(filename)
                candidate_relative_to_device = candidate.relative_to(path)
                if _should_import_file_for_device(
                    device_config, candidate_relative_to_device
                ):
                    files.append(candidate)

        base_commit = "refs/heads/config"
        message = f"Snapshot of '{device_config.name}'"
        ref = device_ref(device_config.name)

        # In order to create the manifest, we first make a tree which is never
        # used. The unused trees are eventually cleaned up by `git gc`.
        with self.git.commit_builder(path) as builder:
            builder.stage_files_from_commit(base_commit)
            builder.stage_files_from_work_tree(files)
            tree_1 = builder.create_tree()
            builder.stage_manifest(tree_1)
            tree_2 = builder.create_tree()
            commit_id = builder.create_commit(tree_2, ref, message)

        self.git.update_ref(ref, commit_id)

    def update_union(self, devices: [DeviceConfig]):
        """Update the "all" ref after a new import."""

        target_ref = "refs/heads/union"
        message = "Update union."
        with self.git.commit_builder() as builder:
            for d in devices:
                source_ref = device_ref(d.name)
                if self.git.ref_exists(source_ref):
                    files = self.list_tree(source_ref, recursive=True)
                    for file in files:
                        if file.info.file_type == FileType.FILE:
                            path = filename_for_union(d.name, file.info.path)
                            builder.stage_committed_file(file.info.content_hash, path)
            tree_1 = builder.create_tree()
            builder.stage_manifest(tree_1)
            tree_2 = builder.create_tree()
            commit_id = builder.create_commit(tree_2, target_ref, message)
        self.git.update_ref(target_ref, commit_id)
