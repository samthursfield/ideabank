# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from io import BufferedReader
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List, Optional
import contextlib
import copy
import logging
import os
import shutil
import subprocess

from .errors import GitError
from .git_lfs_file_info import GitLfsFileInfo
from .manifest import Manifest
from ideabank.store import FileType

log = logging.getLogger(__name__)


def _find_command(name) -> str:
    command = shutil.which(name)
    if command:
        return command
    raise GitError(f"Required command '{name}' was not found.")


def _file_type_from_git_object_type(object_type) -> FileType:
    if object_type == "blob":
        return FileType.FILE
    elif object_type == "tree":
        return FileType.DIRECTORY
    else:
        raise GitError(f"Unexpected Git object type: {object_type}")


class GitLfsHelper:
    """Wrapper around `git` commandline interface, for a specific repo."""

    def __init__(self, path, user_name, user_email, extra_env=None):
        self.path = path
        self.user_name = user_name
        self.user_email = user_email
        self._extra_env = extra_env

        self._git_command = _find_command("git")
        _find_command("git-lfs")

    def with_env(self, extra_env) -> object:
        """Create a copy of the current helper, with a modified environment.

        Useful for working with a temporary index and work tree set via
        GIT_INDEX_FILE and GIT_WORK_TREE environment variables.

        """
        clone = copy.copy(self)
        clone._extra_env = extra_env
        return clone

    def run(self, args, extra_env=None, feed_stdin=None) -> str:
        """Run `git` with the given config.."""
        command = [self._git_command] + args

        real_extra_env = dict()
        if self._extra_env:
            real_extra_env.update(self._extra_env)
        if extra_env:
            real_extra_env.update(extra_env)

        log.debug(f"Run git: {command}, extra_env={real_extra_env}")

        env = copy.deepcopy(os.environ)
        if real_extra_env:
            for k, v in real_extra_env.items():
                env[k] = str(v)
        input_text = None
        if feed_stdin:
            input_text = feed_stdin.encode()

        result = subprocess.run(
            command,
            cwd=self.path,
            env=env,
            input=input_text,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        if result.returncode != 0:
            raise GitError(
                f"Git command failed: {command}. Error output: {result.stderr.decode().strip()}"
            )
        return result.stdout.decode().strip()

    def ref_exists(self, ref) -> bool:
        """Returns True if `ref` exists in the repo."""
        command = [self._git_command, "show-ref", "--verify", "--quiet", ref]
        result = subprocess.run(command, cwd=self.path)
        if result.returncode == 0:
            return True
        return False

    def create_repo(self):
        """Create an empty Git repo at the configured path."""
        try:
            self.path.mkdir()
        except FileExistsError as e:
            raise RuntimeError(
                f"Something exists at {self.path} already and it doesn't look like an ideabank store."
            )
        self.run(["init", "--bare", "."])

    def ls_tree(
        self, treeish: str, subdir: str = "", recursive: bool = False
    ) -> List[GitLfsFileInfo]:
        """List files in a tree.

        The `treeish` can be a tree SHA, a commit SHA or a ref name.

        To list contents of the index, pass `treeish=None` and `index=True`.

        """

        command = ["ls-tree", "-z", "--full-name"]
        command.append(treeish)

        subdir = str(subdir)
        if not subdir.endswith("/"):
            subdir += "/"
        command.append(subdir)

        if recursive:
            # Recurse into subtrees.
            command.append("-r")
            # Show trees when recursing.
            command.append("-t")

        lines = self.run(command)

        for line in lines.strip().split("\0"):
            if len(line) > 0:
                mode, kind, sha1, path = line.split(maxsplit=3)
                yield {
                    "mode": mode,
                    "kind": kind,
                    "hash": sha1,
                    "path": path,
                }

    def commit_metadata(self) -> dict:
        """Return env vars to set appropriate commit metadata."""
        return {
            "GIT_AUTHOR_NAME": self.user_name,
            "GIT_AUTHOR_EMAIL": self.user_email,
            "GIT_COMMITTER_NAME": self.user_name,
            "GIT_COMMITTER_EMAIL": self.user_email,
        }

    def tree_from_commit(self, ref_or_commit: str) -> str:
        command = ["cat-file", "-p", ref_or_commit]
        try:
            lines = self.run(command)
        except GitError as e:
            log.debug(
                f"tree_from_commit: Assuming ref {ref_or_commit} does not "
                f"exist, got: {e}"
            )
            raise FileNotFoundError
        try:
            line = lines.splitlines()[0]
            if not line.startswith("tree "):
                raise GitError(f"Unexpected Git output: {line}")
            tree = line[5:].strip()
        except IndexError as e:
            raise GitError(f"Unable to get tree for commit {ref_or_commit}: {e}")
        return tree

    def update_ref(self, ref: str, commit_id: str):
        """Update `ref` to point to `commit_id`"""
        self.run(["update-ref", ref, commit_id])

    def setup_repo(self, track_types, config_branch_name="config"):
        """Setup an empty, bare repo for use with Git LFS."""

        extra_env = {
            "GIT_DIR": self.path,
        }

        # Install git-lfs hooks
        self.run(["lfs", "install", "--local"], extra_env=extra_env)

        # Create a config branch with Git LFS configuration.
        with TemporaryDirectory() as workdir:
            extra_env["GIT_WORK_TREE"] = workdir

            # Update `.gitattributes' with desired config.
            for pattern in track_types:
                self.run(["lfs", "track", pattern], extra_env=extra_env)

            # Commit the new git-lfs configuration to config branch.
            self.run(["add", ".gitattributes"], extra_env=extra_env)

            extra_env.update(self.commit_metadata())
            self.run(["checkout", "-b", config_branch_name], extra_env=extra_env)
            self.run(["commit", "-m", "Initial configuration"], extra_env=extra_env)

    def smudge(self, lfs_descriptor) -> BufferedReader:
        """Read a content descriptor and stream the LFS binary content."""
        p = subprocess.Popen(
            [self._git_command, "lfs", "smudge"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            cwd=self.path,
        )

        p.stdin.write(lfs_descriptor.encode() + b"\n")
        p.stdin.close()
        return p.stdout

    def get_lfs_blob_size(
        self,
        path: str,
        ref: str,
    ) -> int:
        """Get size of a Git LFS file."""
        obj = f"{ref}:{path}"
        log.debug("Get size of %s", obj)
        lfs_descriptor = self.run(["cat-file", "blob", obj])
        lines = lfs_descriptor.splitlines()
        assert lines[0] == "version https://git-lfs.github.com/spec/v1"
        size = int(lines[2].split(" ")[1])
        return size

    def create_manifest(self, treeish: str) -> Manifest:
        """Generate a manifest of a tree containing Git LFS objects."""
        log.debug("Creating manifest for tree %s", treeish)
        tree_infos = self.ls_tree(treeish, subdir=".", recursive=True)
        file_infos = []
        for info in tree_infos:
            if Path(info["path"]).name.startswith("."):
                continue
            if info["kind"] == "blob":
                file_type = FileType.FILE
                # We assume all files are LFS objects.
                file_size = self.get_lfs_blob_size(
                    info["path"],
                    ref=treeish,
                )
            elif info["kind"] == "tree":
                file_type = FileType.DIRECTORY
                file_size = 0

            file_infos.append(
                GitLfsFileInfo(
                    path=info["path"],
                    file_type=file_type,
                    size=file_size,
                    # We use the hash of the Git LFS pointer, not the actual
                    # OID of the content.
                    content_hash=info["hash"],
                )
            )
        return Manifest(file_infos)

    @contextlib.contextmanager
    def commit_builder(self, work_tree: Path = None) -> object:
        """Context manager which returns a GitCommitBuilder."""
        builder = GitLfsCommitBuilder(self, work_tree)
        try:
            yield builder
        finally:
            builder.cleanup()


class GitLfsCommitBuilder:
    """Helper to create a Git commit with LFS objects for a bare repo."""

    def __init__(self, git: GitLfsHelper, work_tree: Path = None):
        """Use GitHelper.commit_builder() to construct this class.

        The `work_tree` path must be set if you plan to call
        stage_files_from_work_tree().

        """
        # Use a private index file so this operation can be parallelized safely
        self.tmpdir = TemporaryDirectory()
        log.debug(f"Created private workdir at {self.tmpdir}")

        env = {
            "GIT_INDEX_FILE": Path(self.tmpdir.name).joinpath("index"),
        }

        if work_tree:
            env["GIT_WORK_TREE"] = work_tree

        self.git = git.with_env(env)

    def stage_files_from_commit(self, ref):
        """Stage all files from an existing commit.

        The same filenames will be used in the new commit.

        """
        contents = self.git.run(["ls-tree", "-z", "-r", "--full-name", ref])
        self.git.run(["update-index", "-z", "--index-info"], feed_stdin=contents)

    def stage_committed_file(self, object_id, path, mode="100644"):
        """Stage a file that is already in the object store.

        The file will be staged at the given `path`, with the given mode.

        """
        self.git.run(["update-index", "--add", "--cacheinfo", mode, object_id, path])

    def stage_files_from_work_tree(self, files):
        """Add each file to the index. This can be slow for large files."""
        for file in files:
            log.debug(f"Add {file} to index")
            self.git.run(
                ["update-index", "--add", "--remove", file],
            )

    def create_tree(self) -> str:
        """Create a tree from the files staged in the index.

        Returns the tree hash.

        """
        tree_hash = self.git.run(["write-tree"])
        log.debug(f"Wrote tree with hash {tree_hash}")
        return tree_hash

    def stage_manifest(self, tree):
        """Create and stage a manifest file based on `tree`."""
        manifest = self.git.create_manifest(tree)

        with TemporaryDirectory() as d:
            manifest_path = Path(d).joinpath("manifest.json")
            manifest.save(manifest_path)
            self.git.run(
                ["update-index", "--add", "manifest.json"],
                extra_env={"GIT_WORK_TREE": d},
            )

    def create_commit(self, tree_hash, parent, message):
        parent_args = []
        if self.git.ref_exists(parent):
            parent_args = ["-p", parent]

        # Create a commit in the Git object store.
        commit_hash = self.git.run(
            ["commit-tree", tree_hash] + parent_args,
            feed_stdin=message,
            extra_env=self.git.commit_metadata(),
        )
        return commit_hash

    def cleanup(self):
        self.tmpdir.cleanup()
