# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
import dataclasses

from ideabank.store import FileType


@dataclasses.dataclass
class GitLfsFileInfo:
    """Manifest information about a specific file."""

    path: Path
    file_type: FileType
    size: int
    content_hash: str

    def export(self) -> object:
        return {
            "path": str(self.path),
            "file_type": self.file_type.value,
            "size": self.size,
            "content_hash": self.content_hash,
        }
