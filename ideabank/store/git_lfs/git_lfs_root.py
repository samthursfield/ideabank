# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from typing import Iterable
import dataclasses
import logging

from .git_lfs_file import GitLfsFile
from .manifest import Manifest
from ideabank.store import StoreObject

log = logging.getLogger(__name__)


@dataclasses.dataclass
class GitLfsRoot(GitLfsFile):
    """The root of a tree."""

    manifest: Manifest

    def list_tree(self, subdir="./", recursive=False) -> Iterable[StoreObject]:
        """List all paths in the tree that `ref_or_commit` points to.

        Hidden files are not returned by this method.

        Raises FileNotFoundError if `ref_or_commit` is not found.

        """
        log.debug("%s: list_tree(subdir=%s, recursive=%s)", self, subdir, recursive)

        current_depth = len(Path(subdir).parts)
        min_depth = current_depth + 1
        max_depth = 99999 if recursive else min_depth

        for entry in self.manifest.entries:
            if entry.path.is_relative_to(subdir):
                entry_depth = len(entry.path.parts)
                if entry_depth >= min_depth and entry_depth <= max_depth:
                    yield GitLfsFile(self.git, entry, self.ref, root=self)
