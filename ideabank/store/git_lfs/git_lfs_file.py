# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

from io import BufferedReader
from pathlib import Path
from typing import Iterable
import dataclasses
import logging

from .errors import GitError
from .helpers import GitLfsHelper
from .git_lfs_file_info import GitLfsFileInfo
from ideabank.store import FileType, StoreObject

log = logging.getLogger(__name__)


@dataclasses.dataclass
class GitLfsFile(StoreObject):
    """Represents a single file stored using Git LFS."""

    git: GitLfsHelper

    info: GitLfsFileInfo

    ref: str
    root: object  # GitLfsRoot

    def __str__(self) -> str:
        return f"GitLfsFile({self.get_path()})"

    def get_path(self) -> Path:
        return self.info.path

    def get_size(self) -> int:
        return self.info.size

    def read(self) -> BufferedReader:
        obj = f"{self.ref}:{self.get_path()}"
        lfs_descriptor = self.git.run(["cat-file", "blob", obj])
        return self.git.smudge(lfs_descriptor)

    def is_file(self) -> bool:
        return self.info.file_type == FileType.FILE

    def list_tree(self, subdir="./", recursive=False) -> Iterable[StoreObject]:
        subdir = self.get_path().joinpath(subdir)
        return self.root.list_tree(subdir, recursive)
