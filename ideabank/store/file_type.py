# SPDX-FileCopyrightText: 2023  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later


from enum import Enum


class FileType(Enum):
    """Types of file that the store can contain."""

    FILE = 0
    DIRECTORY = 1
