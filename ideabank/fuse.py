"""
The virtual file system for browsing the idea bank.
"""

from datetime import datetime
from io import BufferedReader
from pathlib import Path
from typing import *
import dataclasses
import errno
import logging
import os
import stat
import sys

from fuse import FUSE, FuseOSError, Operations, fuse_get_context

from .config import Config, DeviceConfig, Device
from .store import FileType, Store, StoreObject
from .tree import IdeaBankNode

log = logging.getLogger(__name__)


class FuseFile:
    """A single file or directory in our FUSE file system."""

    def __init__(self, node: IdeaBankNode, store_object: Optional[StoreObject]):
        self._node = node
        self._file = store_object

    def get_size(self) -> int:
        if self._file:
            return self._file.get_size()
        return 0

    def readdir(self) -> List[str]:
        return self._node.get_children()

    def open(self) -> BufferedReader:
        return self._file.read()

    def is_file(self) -> bool:
        return self._node.is_file()


@dataclasses.dataclass
class FuseFileHandle:
    fd: int
    stream: BufferedReader
    pos: int


class IdeaBankFuseOperations(Operations):
    def __init__(self, tree: IdeaBankNode):
        self._debug("Started IdeaBankFUSE")
        self.tree = tree

        self.fixed_time = int(datetime.now().timestamp())

        self.fd = 2
        self.fds = {}

    def _debug(self, message):
        # The Python 'logging' and 'multiprocessing' modules work badly
        # together. Here you can enable print() debugging when needed.
        # print(message)
        log.debug(message)

    def _get(self, path_str) -> FuseFile:
        # Return FuseFile for a given path.
        path = Path(path_str)
        node = self.tree.get_child(path)
        if node:
            return FuseFile(node, store_object=node.get_store_object())
        else:
            raise FuseOSError(errno.ENOENT)

    # Filesystem methods
    # ==================

    def access(self, path, mode: int):
        self._debug(f"access({path})")
        return None

    def chmod(self, path, mode):
        self._debug(f"chmod({path})")
        raise FuseOSError(errno.EACCESS)

    def chown(self, path, uid, gid):
        self._debug(f"chown({path})")
        raise FuseOSError(errno.EACCESS)

    def getattr(self, path, fh=None):
        self._debug(f"getattr: {path}")
        file = self._get(path)
        if file.is_file():
            mode = stat.S_IFREG | 0o755
            size = file.get_size()
        else:
            mode = stat.S_IFDIR | 0o755
            size = 0

        result = {
            "st_atime": self.fixed_time,
            "st_ctime": self.fixed_time,
            "st_gid": os.getgid(),
            "st_mode": mode,
            "st_mtime": self.fixed_time,
            "st_nlink": 0,
            "st_size": size,
            "st_uid": os.getuid(),
        }
        self._debug(f"getattr result: {result}")
        return result

    def readdir(self, path, fh):
        file = self._get(path)
        self._debug(f"readdir: {path}, fuse_file: {file}")
        dirents = [".", ".."]
        result = dirents + file.readdir()
        self._debug(f"readdir result: {result}")
        return result

    def readlink(self, path):
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    def mknod(self, path, mode, dev):
        self._debug(f"mknod({path})")
        raise FuseOSError(errno.EACCESS)

    def rmdir(self, path):
        self._debug(f"rmdir({path})")
        raise FuseOSError(errno.EACCESS)

    def mkdir(self, path, mode):
        self._debug(f"mkdir({path})")
        raise FuseOSError(errno.EACCESS)

    def statfs(self, path):
        self._debug(f"statfs({path})")
        return {
            "f_bavail": 0,
            "f_bfree": 0,
            "f_blocks": 0,
            "f_bsize": 0,
            "f_favail": 0,
            "f_ffree": 0,
            "f_files": 0,
            "f_flag": 0,
            "f_frsize": 0,
            "f_namemax": 1024,
        }

    def unlink(self, path):
        self._debug(f"unlink({path})")
        raise FuseOSError(errno.EACCESS)

    def symlink(self, name, target):
        self._debug(f"symlink({path})")
        raise FuseOSError(errno.EACCESS)

    def rename(self, old, new):
        self._debug(f"rename({path})")
        raise FuseOSError(errno.EACCESS)

    def link(self, target, name):
        self._debug(f"link({path})")
        raise FuseOSError(errno.EACCESS)

    def utimens(self, path, times=None):
        # FIXME
        self._debug(f"utimens({path})")
        raise FuseOSError(errno.EACCESS)

    # File methods
    # ============

    def open(self, path, flags):
        self._debug(f"open({path},{flags})")
        if flags == 32768:
            stream = self._get(path).open()
            self.fd += 1
            self.fds[self.fd] = FuseFileHandle(self.fd, stream, 0)
            return self.fd
        else:
            raise FuseOSError(errno.EACCES)

    def create(self, path, mode, fi=None):
        self._debug(f"create({path})")
        raise FuseOSError(errno.EACCESS)

    def read(self, path, length, offset, fd):
        self._debug(f"read({path},{length},{offset},{fd})")
        if fd in self.fds:
            handle = self.fds[fd]

            if offset < handle.pos:
                # Seek backwards
                handle.stream.close()
                handle.stream = self._get(path).open()
                handle.pos = 0

            if offset > handle.pos:
                # Seek forwards
                skip = offset - handle.pos
                self._debug(f"Skip {skip} bytes")
                garbage = handle.stream.read(skip)
                del garbage

            buf = handle.stream.read(length)
            handle.pos = offset + len(buf)
            return buf
        else:
            raise FuseOSError(errno.EBADFD)

    def write(self, path, buf, offset, fh):
        self._debug(f"write({path})")
        raise FuseOSError(errno.EACCES)

    def truncate(self, path, length, fh=None):
        self._debug(f"truncate({path})")
        raise FuseOSError(errno.EACCES)

    def flush(self, path, fh):
        self._debug(f"flush({path})")
        # This can basically do nothing, see:
        # https://libfuse.github.io/doxygen/structfuse__operations.html#ad4ec9c309072a92dd82ddb20efa4ab14

    def release(self, path, fd):
        self._debug(f"release({path})")
        if fd in self.fds:
            del self.fds[fd]
        else:
            raise FuseOSError(errno.EBADFD)

    def fsync(self, path, fdatasync, fh):
        pass


def run(tree: IdeaBankNode, mountpoint, root):
    log.debug(
        f"Starting FUSE filesystem. Tree {tree}, mountpoint: {mountpoint}, root: {root}"
    )
    ops = IdeaBankFuseOperations(tree)
    FUSE(ops, str(mountpoint), nothreads=True, foreground=True)
