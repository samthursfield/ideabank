from typing import Dict, Iterable
import logging
import os
from pathlib import Path
import pydbus
import sys

from .config import DeviceConfig, Device

log = logging.getLogger(__name__)


def _udisks_drive_matches_config(drive: dict, config: DeviceConfig) -> bool:
    if not config.match_vendor and not config.match_model:
        log.debug(
            f"_udisks_drive_matches_config: Ignoring config entry {config} which has no vendor or model to match against."
        )
        return False

    props = drive["org.freedesktop.UDisks2.Drive"]
    if props.get("Vendor") != config.match_vendor:
        return False
    if props.get("Model") != config.match_model:
        return False
    return True


def _dbus_byte_array_to_path(chars: [int]) -> Path:
    assert chars[-1] == 0
    return Path(os.fsdecode(bytes(chars[:-1])))


class LinuxUdisksVolumeMonitor:
    BUS_NAME = "org.freedesktop.UDisks2"
    BASE_PATH = "/org/freedesktop/UDisks2"
    BASE_IFACE = "org.freedesktop.UDsisk2"

    def __init__(self):
        self.bus = pydbus.SystemBus()

    def find_devices(self, configs: [DeviceConfig]) -> Dict[str, Device]:
        udisks = self.bus.get(
            "org.freedesktop.UDisks2", object_path="/org/freedesktop/UDisks2"
        )
        objects = udisks.GetManagedObjects()

        drives = {}
        block_devices = {}
        for path, obj in objects.items():
            if path.startswith("/org/freedesktop/UDisks2/block_devices/"):
                block_devices[path] = obj
            elif path.startswith("/org/freedesktop/UDisks2/drives/"):
                drives[path] = obj

        result = {}
        for path, block_device in block_devices.items():
            log.debug(f"Considering block device: {path}")
            if "org.freedesktop.UDisks2.Filesystem" in block_device:
                mountpoints = [
                    _dbus_byte_array_to_path(b)
                    for b in block_device["org.freedesktop.UDisks2.Filesystem"][
                        "MountPoints"
                    ]
                ]

                drive_name = block_device["org.freedesktop.UDisks2.Block"]["Drive"]
                if drive_name in drives:
                    drive = drives[drive_name]
                    for config in configs:
                        if _udisks_drive_matches_config(drive, config):
                            result[config.name] = Device(
                                config=config,
                                drive_id=drive_name,
                                mountpoints=mountpoints,
                            )

        return result
