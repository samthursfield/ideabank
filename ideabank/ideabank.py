# SPDX-FileCopyrightText: 2022  Sam Thursfield <sam@afuera.me.uk>
# SPDX-License-Identifier: GPL-3.0-or-later

"""Ideabank

Import and manage audio from musical hardware.

"""

from pathlib import Path
from typing import Dict, Iterable, Optional
import click
import fnmatch
import json
import logging
import os
import sys

from .config import Config, DeviceConfig, Device
from .store import GitLfsStore
from .tree import Toplevel
from . import fuse

log = logging.getLogger(__name__)


@click.group()
@click.option("-c", "--config", type=click.Path(), help="Path to configuration file")
@click.option(
    "-r", "--repo", type=click.Path(), help="Path to repository (overrides config file)"
)
@click.option("--debug", is_flag=True, help="Enable debug logging")
@click.pass_context
def cli(ctx, config, repo, debug):
    if debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    else:
        logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

    ctx.ensure_object(dict)
    ctx.obj["config"] = Config(config, repo_path=repo)


@cli.command(name="export")
@click.argument("idea", type=click.Path())
@click.argument("dest", type=click.File("wb"))
@click.pass_context
def export(ctx, idea, dest):
    """Export an audio file from the idea bank."""

    config = ctx.obj["config"]
    store = GitLfsStore(config.repo_path())
    store.ensure_exists()

    toplevel = Toplevel(store, config)
    node = toplevel.get_child(idea)
    if node is None:
        ctx.fail(f"Invalid idea path: {idea}")
    store_object = node.get_store_object()

    for data in store_object.read():
        dest.write(data)


@cli.command(name="import")
@click.option(
    "--device", "device_name", type=str, help="Name a specific device to import"
)
@click.option("--path", type=click.Path(), help="Mountpount of the specific device")
@click.pass_context
def import_(ctx, device_name, path):
    """Import audio from music hardware.

    By default, devices are autodetected based on configured hardware metadata.

    Alternately you can pass `--device` and `--path` to import a specific
    device and skip the autodetection.

    """
    if device_name and not path or path and not device_name:
        raise RuntimeError("--device and --path must be specified together.")

    config = ctx.obj["config"]

    store = GitLfsStore(config.repo_path())
    store.ensure_exists()

    if path:
        device_path = Path(path).absolute()
    else:
        device_path = None

    imported_something = False

    if device_name:
        device_config = config.get_device_by_name(device_name)
        store.import_from_device(device_config, device_path)
        imported_something = True
    else:
        # This pulls in pydbus which requires gobject-introspection.
        # That's not available inside Tox. So we only import it when needed.
        from .linux_udisks_volume_monitor import LinuxUdisksVolumeMonitor

        volume_monitor = LinuxUdisksVolumeMonitor()
        devices = volume_monitor.find_devices(config.devices().values())
        for device in devices.values():
            imported_something = True
            store.import_from_device(device.config, device.mountpoints[0])

    if imported_something:
        store.update_union(config.devices().values())


@cli.command(name="list")
@click.argument("PATH", required=False, type=click.Path())
@click.option("-d", "--depth", help="Limit recursion depth")
@click.pass_context
def list(ctx, path: Optional[click.Path], depth: Optional[int]):
    """List contents of the idea bank."""
    config = ctx.obj["config"]

    store = GitLfsStore(config.repo_path())
    store.ensure_exists()

    toplevel = Toplevel(store, config)
    if path:
        tree = toplevel.get_child(path)
    else:
        tree = toplevel
    current_depth = 0

    json.dump(tree.export(max_depth=depth), sys.stdout, indent=4)


@cli.command()
@click.argument("mountpoint", type=click.Path())
@click.pass_context
def mount(ctx, mountpoint):
    """Mount idea bank as a FUSE filesystem."""
    config = ctx.obj["config"]

    store = GitLfsStore(config.repo_path())
    store.ensure_exists()

    toplevel = Toplevel(store, config)
    fuse.run(toplevel, mountpoint, "/")


@cli.command()
@click.argument("idea", type=click.Path())
@click.pass_context
def play(ctx, idea):
    """Export an audio file from the idea bank."""

    from .player.gstreamer import GStreamerPlayer

    config = ctx.obj["config"]
    store = GitLfsStore(config.repo_path())
    store.ensure_exists()

    toplevel = Toplevel(store, config)
    node = toplevel.get_child(idea)
    if node is None:
        ctx.fail(f"Invalid idea path: {idea}")
    store_object = node.get_store_object()

    player = GStreamerPlayer()
    player.play(store_object.read())
