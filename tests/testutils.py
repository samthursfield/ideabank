# Copyright (C) 2017-2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Helpers around click.testing module to provide better errors."""


import click.testing

from pathlib import Path
import io
import json
import logging
import os
import pathlib
import shutil
import subprocess
import sys
import threading
import time
import traceback

import ideabank


class CliResult:
    def __init__(self, click_result):
        self.click_result = click_result
        self.stdout = click_result.stdout_bytes.decode("utf-8")
        if click_result.stderr_bytes:
            self.stderr = click_result.stderr_bytes.decode("utf-8")
        else:
            self.stderr = ""
        logging.debug("Got stdout: %s" % self.stdout)
        logging.debug("Got stderr: %s" % self.stderr)
        self.exit_code = click_result.exit_code
        self.exception = click_result.exception

    def assert_success(self, fail_message=None):
        if self.click_result.exit_code != 0:
            if fail_message:
                raise AssertionError(fail_message)
            else:
                sys.stderr.write(f"Exception: {repr(self.click_result.exception)}\n")
                sys.stderr.write(
                    "".join(
                        traceback.format_tb(self.click_result.exception.__traceback__)
                    )
                )
                sys.stderr.write(f"Error output: {self.click_result.stderr}\n")
                raise AssertionError(
                    f"Subprocess failed with code {self.exit_code}.\n"
                    f"Exception: {repr(self.click_result.exception)}.\n"
                    + "".join(
                        traceback.format_tb(self.click_result.exception.__traceback__)
                    )
                )


class Cli:
    def __init__(self, prepend_args=None, extra_env=None, isolate_xdg_dirs=True):
        self.prepend_args = prepend_args or []
        self.extra_env = extra_env or {}
        self.isolate_xdg_dirs = isolate_xdg_dirs

    def run(self, args, input=None):
        cli_runner = click.testing.CliRunner(mix_stderr=False)
        with cli_runner.isolated_filesystem():
            env = os.environ.copy()
            testdir = pathlib.Path(".")

            if self.isolate_xdg_dirs:
                env["XDG_CACHE_HOME"] = str(testdir.absolute())
                env["XDG_CONFIG_HOME"] = str(testdir.absolute())
            if self.extra_env:
                env.update(self.extra_env)

            result = cli_runner.invoke(
                ideabank.cli, self.prepend_args + args, input=input, env=env
            )
        return CliResult(result)


def assert_is_git_repo(path: Path) -> bool:
    """Assert that there is a valid Git repo at 'path'"""
    assert path.joinpath("config").exists()

    git = shutil.which("git")
    result = subprocess.run([git, "branch"], stdout=subprocess.PIPE, cwd=path)
    result.check_returncode()


def await_path(path: Path, timeout=2):
    """Wait until a path is created."""
    # This should be done using a filesystem monitor, but I am in a hurry.
    start = time.time()
    while not path.exists():
        time.sleep(0.2)
        if time.time() > (start + timeout):
            raise RuntimeError(f"Path {path} did not appear after {timeout}s")
