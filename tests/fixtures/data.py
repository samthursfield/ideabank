"""Fake data for use by tests."""


CONFIG_TEMPLATE = """
[config]
# Location of the idea bank database.
path="{database_path}"

[device.device_1]
type="storage"
include="/audio/*.wav"
"""


TEST_AUDIO_DATA_1 = "I am a wav file"
TEST_AUDIO_DATA_2 = "Yo soy audio"
