"""
Fixture providing a fake external device with some fake content.

This is useful to test the import process.
"""


from .data import TEST_AUDIO_DATA_1, TEST_AUDIO_DATA_2

import pytest


@pytest.fixture
def example_device_1(tmp_path):
    """Create some files that we might find on a real music device."""
    device_dir = tmp_path.joinpath("device_1")

    data_dir = device_dir.joinpath("audio")
    data_dir.mkdir(parents=True)
    data_dir.joinpath("1.wav").write_text(TEST_AUDIO_DATA_1)
    data_dir.joinpath("2.wav").write_text(TEST_AUDIO_DATA_2)
    return device_dir
