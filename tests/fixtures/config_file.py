"""Fixture providing a `config.yaml` file based on data.CONFIG_TEMPLATE"""

import pytest

from .data import CONFIG_TEMPLATE


@pytest.fixture
def config_file(tmp_path):
    database_path = tmp_path.joinpath("ideabank")
    text = CONFIG_TEMPLATE.format(database_path=database_path)
    path = tmp_path.joinpath("config.toml")
    path.write_text(text)
    return path
