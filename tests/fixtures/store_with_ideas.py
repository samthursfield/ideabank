"""Fixture providing a ready-populated IdeaBank store."""

import pytest

from testutils import Cli, assert_is_git_repo


@pytest.fixture
def store_with_ideas(tmp_path, config_file, example_device_1):
    database_path = tmp_path.joinpath("ideabank")

    # Run the import and assert it did something.
    cli = Cli()
    result = cli.run(
        [
            "--config",
            config_file,
            "import",
            "--device",
            "device_1",
            "--path",
            example_device_1,
        ]
    )
    result.assert_success()

    assert_is_git_repo(database_path)

    return config_file
