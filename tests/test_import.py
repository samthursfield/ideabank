"""Tests for ideabank `import` command."""

import json

from fixtures.data import CONFIG_TEMPLATE
from .testutils import Cli, assert_is_git_repo


def config_text(database_path):
    """Create text for a TOML config file"""
    return CONFIG_TEMPLATE.format(database_path=database_path)


def test_import_specific_device(tmp_path, example_device_1):
    """Import audio files from a single device, and list the results."""

    database_path = tmp_path.joinpath("ideabank")

    config = tmp_path.joinpath("config.toml")
    config.write_text(config_text(database_path))

    cli = Cli()
    import_result = cli.run(
        [
            "--config",
            config,
            "import",
            "--device",
            "device_1",
            "--path",
            example_device_1,
        ]
    )
    import_result.assert_success()

    assert_is_git_repo(database_path)

    list_result = cli.run(["--config", config, "list"])
    list_result.assert_success()

    list_output = json.loads(list_result.stdout)
    assert list_output == {
        "devices": {"device_1": {"audio": {"files": ["1.wav", "2.wav"]}, "files": []}},
        "ideas": {"files": ["device_1-audio-1.wav", "device_1-audio-2.wav"]},
        "labels": {},
    }
