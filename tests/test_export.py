"""Tests for ideabank `export` command."""

import json

from fixtures.data import CONFIG_TEMPLATE, TEST_AUDIO_DATA_1
from .testutils import Cli, assert_is_git_repo


def test_export_idea(tmp_path, store_with_ideas):
    """Export a single idea."""

    config_path = store_with_ideas

    cli = Cli()
    export_result = cli.run(
        [
            "--config",
            config_path,
            "export",
            "ideas/device_1-audio-1.wav",
            "-",
        ]
    )
    export_result.assert_success()

    assert export_result.stdout == TEST_AUDIO_DATA_1
