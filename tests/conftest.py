"""Testsuite configuration."""


from pathlib import Path
import sys

tests_dir = str(Path(__file__).parent)
sys.path.insert(0, tests_dir)

print(sys.path)

import fixtures

import pytest


pytest_plugins = [
    "fixtures.config_file",
    "fixtures.example_device_1",
    "fixtures.store_with_ideas",
]
