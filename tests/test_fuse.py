"""Test for ideabank `mount` command."""

from pathlib import Path
from multiprocessing import Process
import logging
import sys

import pytest

from ideabank.config import Config
from ideabank.store import GitLfsStore
from ideabank.tree import Toplevel
import ideabank.fuse

from fixtures.data import TEST_AUDIO_DATA_1, TEST_AUDIO_DATA_2
from .testutils import Cli, await_path


def run_fuse_process(config_path: Path, mountpoint: Path):
    """Run the FUSE filesystem in a process"""
    # FIXME: logging is eaten.
    #  multiprocessing.log_to_stderr() doesn't fix it.
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    config = ideabank.config.Config(config_path)
    store = GitLfsStore(config.repo_path())

    toplevel = Toplevel(store, config)
    ideabank.fuse.run(toplevel, mountpoint, "/")


def test_browse_fuse(tmp_path, store_with_ideas):
    """Import audio files from a simple device, and browse the results."""

    config_path = store_with_ideas

    # Mount the FUSE filesystem
    # multiprocessing.log_to_stderr(logging.DEBUG)
    mountpoint = tmp_path.joinpath("fs")
    mountpoint.mkdir()

    fuse_process = Process(
        target=run_fuse_process, args=(config_path, mountpoint), daemon=True
    )
    fuse_process.start()
    try:
        await_path(mountpoint.joinpath("devices"))

        # Test the expected files are present and valid
        device_1_audio_dir = mountpoint.joinpath("devices/device_1/audio")
        device_1_audio_files = list(device_1_audio_dir.iterdir())
        assert len(device_1_audio_files) == 2

        assert device_1_audio_dir.joinpath("1.wav").read_text() == TEST_AUDIO_DATA_1
        assert device_1_audio_dir.joinpath("2.wav").read_text() == TEST_AUDIO_DATA_2
    finally:
        fuse_process.terminate()
        fuse_process.join()

    assert fuse_process.exitcode == 0
