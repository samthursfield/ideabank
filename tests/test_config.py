"""Config tests for ideabank."""

from .testutils import Cli


def test_invalid_config(tmp_path):
    """Invalid TOML in config file should return an error."""

    config = tmp_path.joinpath("config.toml")
    config.write_text("Bananas are delicious")

    cli = Cli()
    result = cli.run(["--config", config, "import"])
    assert result.exit_code != 0
