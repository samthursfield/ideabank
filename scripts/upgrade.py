"""Helper to upgrade store version 0 to store version 1."""

from tempfile import TemporaryDirectory
import logging
import sys

import click

from ideabank.store import GitLfsStore

log = logging.getLogger()


@click.command()
@click.argument("store_path")
@click.option("--branch", "-b", help="Branch name")
def main(store_path, branch):
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    store = GitLfsStore(store_path)
    assert branch

    ref = "refs/heads/" + branch
    try:
        manifest = store._get_manifest(ref)
        print(f"Manifest exists in branch {manifest}")
        raise SystemExit
    except RuntimeError as e:
        log.debug(f"No existing manifest, got: {e}")

    with TemporaryDirectory() as d:
        with store.git.commit_builder(d) as builder:
            builder.stage_files_from_commit(branch)
            tree_1 = builder.create_tree()
            builder.stage_manifest(tree_1)
            tree_2 = builder.create_tree()
            commit_id = builder.create_commit(tree_2, ref, "Add manifest")

    print(f"Update ref {ref} to commit {commit_id}")
    store.git.update_ref(ref, commit_id)


main()
